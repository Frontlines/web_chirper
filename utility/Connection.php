<?php

namespace Utility;

use PDO;
use Exception;

class Connection {
	public $pdo;
	private static $connection = null;

	private function __construct() {
		$dsn = "mysql:host=localhost;dbname=projekt;charset=utf8mb4";

		$options = [
			PDO::ATTR_EMULATE_PREPARES => false,
			PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
			PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
		];

		try {
			$this->pdo = new PDO($dsn, "projekt", "projekt", $options);
		} catch (Exception $e) {
			error_log($e->getMessage());
			var_dump($e->getMessage());

			exit('Something weird happened');
		}
	}

	static function singleton() {
		if (self::$connection !== null) {
			return self::$connection;
		}

		$connection = new Connection();
		self::$connection = $connection->pdo;

		return self::$connection;
	}
}
