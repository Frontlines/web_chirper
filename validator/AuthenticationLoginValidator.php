<?php

namespace Validator;

use Repository\UserRepository;

class AuthenticationLoginValidator {
    static function validate($body) {
        $error = [];

        if (!isset($body['email']) || empty($body['email'])) {
            $error['email'] = 'This field is required.';
        } else {
            $user_repository = new UserRepository();
            $user = $user_repository->find($body['email']);
    
            if (!$user) {
                $error['email'] = 'The email does not exist.';
            }
        }

        if (!isset($body['password']) || empty($body['password'])) {
            $error['password'] = 'This field is required.';
        }

        return $error;
    }
}
