<?php

namespace Validator;

class PostCreateValidator {
    static function validate($body) {
        $error = [];

        if (!isset($body['body']) || empty($body['body'])) {
            $error['body'] = 'The body is required.';
        } else {
            if (strlen($body['body']) < 1) {
                $error['body'] = 'The body must contain at least 1 character.';
            } else if (strlen($error['body']) > 280) {
                $error['body'] = 'The body must contain at most 280 characters.';
            }
        }

        return $error;
    }
}
