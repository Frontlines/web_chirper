<?php

namespace Validator;

use Repository\UserRepository;

class AuthenticationRegisterValidator {
    static function validate($body) {
        $error = [];

        if (!isset($body['email']) || empty($body['email'])) {
            $error['email'] = 'The email is required.';
        } else {
            $user_repository = new UserRepository();
            $user = $user_repository->find($body['email']);

            if ($user) {
                $error['email'] = 'The email is already in use.';
            }
        }

        if (!isset($body['handle']) || empty($body['handle'])) {
            $error['handle'] = 'The handle is required.';
        } else {
            $user_repository = new UserRepository();
            $user = $user_repository->find($body['handle']);

            if ($user) {
                $error['handle'] = 'The handle is already in use.';
            } else if (strlen($body['handle']) > 15) {
                $error['handle'] = 'The handle must contain at most 15 characters.';
            }
        }

        if (!isset($body['password']) || empty($body['password'])) {
            $error['password'] = 'The password is required.';
        } else {
            if (strlen($body['password']) < 4) {
                $error['password'] = 'The password must contain at least 4 characters.';
            } else if (strlen($body['password']) > 255) {
                $error['password'] = 'The password must contain at most 255 characters.';
            }
        }

        if (!isset($body['display_name']) || empty($body['display_name'])) {
            $error['display_name'] = 'The display name is required.';
        } else {
            if (strlen($body['display_name']) > 50) {
                $error['display_name'] = 'The display name must contain at most 50 characters.';
            }
        }

        return $error;
    }
}
