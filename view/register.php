<?php include_once dirname(__FILE__) . '/header.php'; ?>

<div class="signup-container text-center">
    <form class="form-signup" action="/register" method="POST">
    <h1 class="h3 mb-3 font-weight-normal">Sign up for Chirper</h1>

<?php if (isset($__error) && count($__error) > 0): ?>
    <p class="text-danger">
    <?php foreach($__error as $key => $value): ?>
      <?php echo $value; ?><br>
    <?php endforeach; ?>
    </p>
<?php endif; ?>

    <label for="inputEmail" class="sr-only">Email</label>
    <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email" required autofocus maxlength="255">

    <label for="inputPassword" class="sr-only">Password</label>
    <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required minlength="4" maxlength="255">

    <label for="inputHandle" class="sr-only">Handle</label>
    <input type="text" name="handle" id="inputHandle" class="form-control" placeholder="Handle" required maxlength="15">

    <label for="inputDisplayName" class="sr-only">Display name</label>
    <input type="text" name="display_name" id="inputDisplayName" class="form-control" placeholder="Display name" required maxlength="50">

    <button class="btn btn-lg btn-primary btn-block" type="submit">Sign up</button>
    <p class="mt-5 mb-3 text-muted text-center">Already have an account? <a href="/login">Sign in</a></p>
    </form>
</div>

<?php include_once dirname(__FILE__) . '/footer.php'; ?>
