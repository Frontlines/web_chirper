<?php include_once dirname(__FILE__) . '/header.php'; ?>

<div class="container my-5">
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-12">
            <ul class="nav flex-lg-column flex-md-column flex-sm-row justify-content-center justify-content-sm-start mb-4 mb-md-0 nav-pills">
            <?php if($__view['is_authenticated']): ?>
                <li class="nav-item">
                    <a class="nav-link" href="/home">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/<?php echo $_SESSION['user_handle']; ?>">My Profile</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/<?php echo $_SESSION['user_handle']; ?>/followers">My Followers</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?php if ($__view['is_authenticated_profile']): echo "active"; endif; ?>" href="/<?php echo $_SESSION['user_handle']; ?>/following">My Follows</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/logout">Sign out</a>
                </li>
            <?php else: ?>
                <li class="nav-item">
                    <a class="nav-link" href="/register">Sign up</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/login">Sign in</a>
                </li>
            <?php endif; ?>
            </ul>
        </div>
        <div class="col-lg-6 col-md-9 col-sm-12">
            <div class="card text-white bg-primary">
                <div class="card-header">
                <h1 class="h3"><a class="text-white" href="/<?php echo $__view['user']->handle ?>"><?php echo $__view['user']->display_name ?></a> <small class="text-white-50">@<?php echo $__view['user']->handle ?></small></h1>
                </div>
                <div class="card-body">
                    <p>Member since <?php echo date("d. M Y.", strtotime($__view['user']->created_at)); ?></p>
                    <div>
                        <a href="/<?php echo $__view['user']->handle; ?>/followers" class="btn btn-light d-block d-sm-inline-block mt-2 mt-sm-0">Followers (<?php echo $__view['follower_count']; ?>)</a>
                        <a href="/<?php echo $__view['user']->handle; ?>/following" class="btn btn-light d-block d-sm-inline-block mt-2 mt-sm-0">Following (<?php echo $__view['followee_count']; ?>)</a>
                    <?php if (!$__view['is_home'] && $__view['is_authenticated'] && !$__view['is_authenticated_profile']): ?>
                        <form class="d-block d-sm-inline-block ml-0 ml-sm-2 form-follow-unfollow mt-2 mt-sm-0" action="/<?php echo $__view['user']->handle; ?>/<?php echo ($__view['is_followed']) ? 'unfollow' : 'follow'; ?>" method="POST">
                            <button type="submit" class="btn btn-light d-block d-sm-inline-block mt-2 mt-sm-0 w-100"><?php echo ($__view['is_followed']) ? 'Unfollow' : 'Follow'; ?></button>
                        </form>
                    <?php endif; ?>
                    </div>
                </div>
            </div>
        <?php if (empty($__view['followees'])): ?>
            <p class="text-center my-4">There's nothing here...</p>
        <?php else: ?>
            <h2 class="h3 my-4">Following <?php echo count($__view['followees']); ?></h2>
        <?php foreach ($__view['followees'] as $followee): ?>
            <div class="card my-3">
                <div class="card-body">
                    <a href="/<?php echo $followee->followee_user->handle; ?>"><?php echo $followee->followee_user->display_name; ?></a> <small><span class="text-muted">@<?php echo $followee->followee_user->handle; ?> &mdash; Since <?php echo date("d. M. Y. ", strtotime($followee->created_at)); ?></span></small>
                </div>
            </div>
        <?php endforeach; ?>
        <?php endif; ?>
        </div>
    </div>
</div>

<?php include_once dirname(__FILE__) . '/footer.php'; ?>
