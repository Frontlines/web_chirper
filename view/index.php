<?php include_once dirname(__FILE__) . '/header.php'; ?>

<div class="home-container text-center">
<div class="cover-container d-flex p-1 h-100 w-100 mx-auto flex-column justify-content-center align-items-center">
  <main role="main" class="inner cover">
    <h1 class="cover-heading">Welcome to Chirper.</h1>
    <p class="lead">Keep in touch with the rest of the world. Sign up now.</p>
    <p class="lead">
        <a href="/register" class="btn btn-lg btn-primary">Sign up</a>
        <a href="/login" class="btn btn-lg btn-secondary">Sign in</a>
    </p>
  </main>
</div>
</div>

<?php include_once dirname(__FILE__) . '/footer.php'; ?>
