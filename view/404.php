<?php include_once dirname(__FILE__) . '/header.php'; ?>

<div class="login-container text-center flex-column justify-content-center">
    <h1 class="my-3 font-weight-normal">404 - Page Not Found</h1>
    <p class="my-3 text-center"><a href="/">Take me home</a></p>
</div>

<?php include_once dirname(__FILE__) . '/footer.php'; ?>
