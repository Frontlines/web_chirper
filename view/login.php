<?php include_once dirname(__FILE__) . '/header.php'; ?>

<div class="login-container text-center">
    <form class="form-signin" action="/login" method="POST">
        <h1 class="h3 mb-3 font-weight-normal">Sign into Chirper</h1>
<?php if (isset($__error) && count($__error) > 0): ?>
    <p class="text-danger">
    <?php foreach($__error as $key => $value): ?>
      <?php echo $value; ?><br>
    <?php endforeach; ?>
    </p>
<?php endif; ?>
        <label for="inputEmail" class="sr-only">Email</label>
        <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email" required autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
        <p class="mt-5 mb-3 text-muted text-center">Don't have an account? <a href="/register">Sign up</a></p>
    </form>
</div>

<?php include_once dirname(__FILE__) . '/footer.php'; ?>
