<?php

namespace Repository;

use Model\UserModel;
use Utility\Connection;
use PDO;

class UserRepository {
    private $connection;

    public function __construct($connection = null) {
        if ($connection !== null) {
            $this->connection = $connection;
        } else {
            $this->connection = Connection::singleton();
        }
    }

    public function find($id) {
        $statement = $this->connection->prepare('
            SELECT user.* from user WHERE id = :id OR email = :email OR handle = :handle
        ');

        $statement->bindParam(':id', $id);
        $statement->bindParam(':email', $id);
        $statement->bindParam(':handle', $id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_CLASS, 'Model\UserModel');

        return $statement->fetch();
    }

    public function all($id) {
        $statement = $this->connection->prepare('
            SELECT * from user
        ');

        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_CLASS, 'Model\UserModel');

        return $statement->fetchAll();
    }

    public function save(UserModel $user) {
        $statement = $this->connection->prepare('
            INSERT INTO user (handle, display_name, email, password, created_at)
            VALUES (:handle, :display_name, :email, :password, FROM_UNIXTIME(:created_at))
        ');

        $statement->bindParam(':handle', $user->handle);
        $statement->bindParam(':display_name', $user->display_name);
        $statement->bindParam(':email', $user->email);
        $statement->bindParam(':password', $user->password);
        $statement->bindParam(':created_at', time());

        return $statement->execute();
    }
}
