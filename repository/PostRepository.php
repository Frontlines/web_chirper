<?php

namespace Repository;

use Utility\Connection;
use Model\PostModel;
use Model\UserModel;
use PDO;

class PostRepository {
    private $connection;

    public function __construct($connection = null) {
        if ($connection !== null) {
            $this->connection = $connection;
        } else {
            $this->connection = Connection::singleton();
        }
    }

    public function find($id) {
        $statement = $this->connection->prepare('
            SELECT post.*,
                user.email as user_email,
                user.password as user_password,
                user.handle as user_handle,
                user.display_name as user_display_name,
                user.created_at as user_created_at
            FROM post
            INNER JOIN user
                ON post.user_id = user.id
            WHERE post.id = :id
        ');

        $statement->bindParam(':id', $id);
        $statement->execute();

        $row = $statement->fetch();

        $user = new UserModel([
            'id' => $row['user_id'],
            'email' => $row['user_email'],
            'password' => $row['user_password'],
            'handle' => $row['user_handle'],
            'display_name' => $row['user_display_name'],
            'created_at' => $row['user_created_at'],
        ]);

        $post = new PostModel([
            'id' => $row['id'],
            'body' => $row['body'],
            'created_at' => $row['created_at'],
            'user' => $user,
        ]);

        return $post;
    }

    public function all() {
        $statement = $this->connection->prepare('
            SELECT post.*,
                user.email as user_email,
                user.password as user_password,
                user.handle as user_handle,
                user.display_name as user_display_name,
                user.created_at as user_created_at
            FROM post
            INNER JOIN user
                ON post.user_id = user.id
            ORDER BY post.id DESC
        ');

        $statement->execute();

        $rows = $statement->fetchAll();

        $posts = [];

        foreach ($rows as $row) {
            $author = new UserModel([
                'id' => $row['user_id'],
                'email' => $row['user_email'],
                'password' => $row['user_password'],
                'handle' => $row['user_handle'],
                'display_name' => $row['user_display_name'],
                'created_at' => $row['user_created_at'],
            ]);
    
            $post = new PostModel([
                'id' => $row['id'],
                'body' => $row['body'],
                'created_at' => $row['created_at'],
                'user' => $author,
            ]);

            $posts[] = $post;
        }

        return $posts;
    }

    public function allFromUser(UserModel $user) {
        $statement = $this->connection->prepare('
            SELECT post.*,
                user.email as user_email,
                user.password as user_password,
                user.handle as user_handle,
                user.display_name as user_display_name,
                user.created_at as user_created_at
            FROM post
            INNER JOIN user
                ON post.user_id = user.id
            WHERE post.user_id = :id
            ORDER BY post.id DESC
        ');

        $statement->bindParam(':id', $user->id);
        $statement->execute();

        $rows = $statement->fetchAll();

        $posts = [];

        foreach ($rows as $row) {
            $post = new PostModel([
                'id' => $row['id'],
                'body' => $row['body'],
                'created_at' => $row['created_at'],
                'user' => $user,
            ]);

            $posts[] = $post;
        }

        return $posts;
    }

    public function allForUser($user) {
        $statement = $this->connection->prepare('
            SELECT post.*,
                user.email as user_email,
                user.password as user_password,
                user.handle as user_handle,
                user.display_name as user_display_name,
                user.created_at as user_created_at
            FROM post
            INNER JOIN user ON post.user_id = user.id
            WHERE post.user_id IN (
                SELECT user_follower.followee_user_id
                FROM user_follower
                WHERE user_follower.follower_user_id = :follower_id
            )
            OR post.user_id = :user_id
            ORDER BY post.id DESC
        ');

        $statement->bindParam(':follower_id', $user->id);
        $statement->bindParam(':user_id', $user->id);
        $statement->execute();

        $rows = $statement->fetchAll();

        $posts = [];

        foreach ($rows as $row) {
            $author = new UserModel([
                'id' => $row['user_id'],
                'email' => $row['user_email'],
                'password' => $row['user_password'],
                'handle' => $row['user_handle'],
                'display_name' => $row['user_display_name'],
                'created_at' => $row['user_created_at'],
            ]);

            $post = new PostModel([
                'id' => $row['id'],
                'body' => $row['body'],
                'created_at' => $row['created_at'],
                'user' => $author,
            ]);

            $posts[] = $post;
        }

        return $posts;
    }

    public function save(PostModel $post) {
        $statement = $this->connection->prepare('
            INSERT INTO post (body, user_id, created_at)
            VALUES (:body, :user_id, FROM_UNIXTIME(:created_at))
        ');

        $statement->bindParam(':body', $post->body);
        $statement->bindParam(':user_id', $post->user->id);
        $statement->bindParam(':created_at', time());

        return $statement->execute();
    }
}
