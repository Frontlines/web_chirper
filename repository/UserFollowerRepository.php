<?php

namespace Repository;

use Utility\Connection;
use Model\UserFollowerModel;
use Model\UserModel;
use PDO;

class UserFollowerRepository {
    private $connection;

    public function __construct($connection = null) {
        if ($connection !== null) {
            $this->connection = $connection;
        } else {
            $this->connection = Connection::singleton();
        }
    }

    public function find($id) {
        $statement = $this->connection->prepare('
            SELECT user_follower.* FROM user_follower WHERE id = :id
        ');

        $statement->bindParam(':id', $id);
        $statement->execute();

        return $statement->fetch();
    }

    public function findFollowedByUser(UserModel $follower, UserModel $followee) {
        $statement = $this->connection->prepare('
            SELECT user_follower.* FROM user_follower
            WHERE follower_user_id = :follower_user_id
                AND followee_user_id = :followee_user_id
        ');

        $statement->bindParam(':follower_user_id', $follower->id);
        $statement->bindParam(':followee_user_id', $followee->id);
        $statement->execute();

        $row = $statement->fetch();

        if (!$row) {
            return null;
        }

        $user_follower = new UserFollowerModel([
            'id' => $row['id'],
            'follower_user' => $follower,
            'followee_user' => $followee,
            'created_at' => $row['created_at'],
        ]);

        return $user_follower;
    }

    public function all($id) {
        $statement = $this->connection->prepare('
            SELECT * FROM user_follower
        ');

        $statement->execute();

        return $statement->fetchAll();
    }

    public function allFollowedByUser(UserModel $user) {
        $statement = $this->connection->prepare('
            SELECT user_follower.*,
                user.id AS user_id,
                user.email AS user_email,
                user.password AS user_password,
                user.handle AS user_handle,
                user.display_name AS user_display_name,
                user.created_at user_created_at
            FROM user_follower
            INNER JOIN user ON user_follower.followee_user_id = user.id
            WHERE user_follower.follower_user_id = :id
            ORDER BY user_follower.id DESC
        ');

        $statement->bindParam(':id', $user->id);
        $statement->execute();

        $rows = $statement->fetchAll();

        $user_followers = [];

        foreach ($rows as $row) {
            $followee_user = new UserModel([
                'id' => $row['user_id'],
                'email' => $row['user_email'],
                'password' => $row['user_password'],
                'handle' => $row['user_handle'],
                'display_name' => $row['user_display_name'],
                'created_at' => $row['user_created_at'],
            ]);

            $user_follower = new UserFollowerModel([
                'id' => $row['id'],
                'follower_user' => $user,
                'followee_user' => $followee_user,
                'created_at' => $row['created_at'],
            ]);

            $user_followers[] = $user_follower;
        }

        return $user_followers;
    }

    public function allFollowingUser(UserModel $user) {
        $statement = $this->connection->prepare('
            SELECT user_follower.*,
                user.id AS user_id,
                user.email AS user_email,
                user.password AS user_password,
                user.handle AS user_handle,
                user.display_name AS user_display_name,
                user.created_at user_created_at
            FROM user_follower
            INNER JOIN user ON user_follower.follower_user_id = user.id
            WHERE user_follower.followee_user_id = :id
            ORDER BY user_follower.id DESC
        ');

        $statement->bindParam(':id', $user->id);
        $statement->execute();

        $rows = $statement->fetchAll();

        $user_followers = [];

        foreach ($rows as $row) {
            $follower_user = new UserModel([
                'id' => $row['user_id'],
                'email' => $row['user_email'],
                'password' => $row['user_password'],
                'handle' => $row['user_handle'],
                'display_name' => $row['user_display_name'],
                'created_at' => $row['user_created_at'],
            ]);

            $user_follower = new UserFollowerModel([
                'id' => $row['id'],
                'follower_user' => $follower_user,
                'followee_user' => $user,
                'created_at' => $row['created_at'],
            ]);

            $user_followers[] = $user_follower;
        }

        return $user_followers;
    }

    public function save(UserFollowerModel $user_follower) {
        $statement = $this->connection->prepare('
            INSERT INTO user_follower (follower_user_id, followee_user_id, created_at)
            VALUES (:follower_user_id, :followee_user_id, FROM_UNIXTIME(:created_at))
        ');

        $statement->bindParam(':follower_user_id', $user_follower->follower_user->id);
        $statement->bindParam(':followee_user_id', $user_follower->followee_user->id);
        $statement->bindParam(':created_at', time());

        return $statement->execute();
    }

    public function remove($id) {
        $statement = $this->connection->prepare('
            DELETE FROM user_follower WHERE user_follower.id = :id
        ');

        $statement->bindParam(':id', $id);
        
        return $statement->execute();
    }

    public function removeByUsers(UserModel $follower, UserModel $followee) {
        $statement = $this->connection->prepare('
            DELETE FROM user_follower
            WHERE user_follower.follower_user_id = :follower_user_id
                AND user_follower.followee_user_id = :followee_user_id
        ');

        $statement->bindParam(':follower_user_id', $follower->id);
        $statement->bindParam(':followee_user_id', $followee->id);
        
        return $statement->execute();
    }
}
