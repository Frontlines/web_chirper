<?php

namespace Model;

class PostModel {
    public $id;
    public $body;
    public $created_at;
    public $user;

    public function __construct($data = null) {
        if (!is_array($data)) {
            return;
        }

        $this->id = isset($data['id']) ? $data['id'] : null;
        $this->body = $data['body'];
        $this->created_at = isset($data['created_at']) ? $data['created_at'] : null;
        $this->user = $data['user'];
    }
}
