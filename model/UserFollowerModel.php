<?php

namespace Model;

class UserFollowerModel {
    public $id;
    public $follower_user;
    public $followee_user;
    public $created_at;

    public function __construct($data = null) {
        if (!is_array($data)) {
            return;
        }

        $this->id = isset($data['id']) ? $data['id'] : null;
        $this->follower_user = $data['follower_user'];
        $this->followee_user = $data['followee_user'];
        $this->created_at = isset($data['created_at']) ? $data['created_at'] : null;
    }
}
