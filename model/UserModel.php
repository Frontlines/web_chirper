<?php

namespace Model;

class UserModel {
    public $id;
    public $email;
    public $password;
    public $handle;
    public $display_name;
    public $created_at;

    public function __construct($data = null) {
        if (!is_array($data)) {
            return;
        }

        $this->id = isset($data['id']) ? $data['id'] : null;
        $this->email = $data['email'];
        $this->password = $data['password'];
        $this->handle = $data['handle'];
        $this->display_name = $data['display_name'];
        $this->created_at = isset($data['created_at']) ? $data['created_at'] : null;
    }
}
