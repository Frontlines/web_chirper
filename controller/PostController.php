<?php

namespace Controller;

use Repository\UserRepository;
use Repository\PostRepository;
use Validator\PostCreateValidator;
use Model\PostModel;

class PostController {
    static function create() {
        if (!isset($_SESSION['user_handle'])) {
            // @todo
            throw "unauthorized";
        }

        $error = PostCreateValidator::validate($_POST);

        if (!empty($error)) {
            $__error = $error;
            
            header('Location: /home');

            return;
        }

        $user_repository = new UserRepository();
        $user = $user_repository->find($_SESSION['user_handle']);

        if (!$user) {
            // @todo
            // @note shouldn't be possible
            throw "no user";
        }

        $post = new PostModel(array_merge($_POST, [ 'user' => $user ]));

        $post_repository = new PostRepository();
        $post_repository->save($post);

        header('Location: /home');

        return;
    }
}
