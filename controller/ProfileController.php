<?php

namespace Controller;

use Repository\UserFollowerRepository;
use Repository\UserRepository;
use Repository\PostRepository;
use Validator\ProfileCreatePostValidator;
use Model\PostModel;
use Model\UserFollowerModel;

class ProfileController {
    static function show($matches) {
        if (!$matches || empty($matches)) {
            header('Location: /404');

            return;
        }

        $handle = $matches[0];

        $user_repository = new UserRepository();
        $user = $user_repository->find($handle);

        if (!$user) {
            header('Location: /404');

            return;
        }

        $post_repository = new PostRepository();
        $posts = $post_repository->allFromUser($user);

        $is_authenticated = isset($_SESSION['user_handle']);
        $is_followed = false;

        $user_follower_repository = new UserFollowerRepository();
        $follower_count = count($user_follower_repository->allFollowingUser($user));
        $followee_count = count($user_follower_repository->allFollowedByUser($user));

        if ($is_authenticated) {
            $auth_user = $user_repository->find($_SESSION['user_handle']);
            $user_follower = $user_follower_repository->findFollowedByUser($auth_user, $user);
            $is_followed = boolval($user_follower);
        }

        $__view = [
            'user' => $user,
            'posts' => $posts,
            'is_authenticated' => $is_authenticated,
            'is_authenticated_profile' => $is_authenticated && $_SESSION['user_handle'] === $user->handle,
            'is_home' => false,
            'is_followed' => $is_followed,
            'follower_count' => $follower_count,
            'followee_count' => $followee_count,
        ];

        require_once dirname(__FILE__) . '/../view/profile.php';

        return;
    }

    static function showHome() {
        if (!isset($_SESSION['user_handle'])) {
            header('Location: /login');

            return;
        }

        $user_repository = new UserRepository();
        $user = $user_repository->find($_SESSION['user_handle']);

        if (!$user) {
            // @note shouldn't be possible
            header('Location: /404');

            return;
        }

        $post_repository = new PostRepository();
        $posts = $post_repository->allForUser($user);

        $user_follower_repository = new UserFollowerRepository();
        $follower_count = count($user_follower_repository->allFollowingUser($user));
        $followee_count = count($user_follower_repository->allFollowedByUser($user));

        $__view = [
            'user' => $user,
            'posts' => $posts,
            'is_authenticated' => isset($_SESSION['user_handle']),
            'is_authenticated_profile' => isset($_SESSION['user_handle']) && $_SESSION['user_handle'] === $user->handle,
            'is_home' => true,
            'is_followed' => false,
            'follower_count' => $follower_count,
            'followee_count' => $followee_count,
        ];

        require_once dirname(__FILE__) . '/../view/profile.php';

        return;
    }
}
