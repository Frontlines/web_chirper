<?php

namespace Controller;

class IndexController {
    static function show() {
        if (isset($_SESSION['user_handle'])) {
            header('Location: /home');

            return;
        }

        require_once dirname(__FILE__) . '/../view/index.php';

        return;
    }
}
