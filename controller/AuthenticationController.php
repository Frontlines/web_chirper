<?php

namespace Controller;

use Validator\AuthenticationLoginValidator;
use Validator\AuthenticationRegisterValidator;
use Model\UserModel;
use Repository\UserRepository;
use ErrorException;

class AuthenticationController {
    static function showLogin() {
        require_once dirname(__FILE__) . '/../view/login.php';

        return;
    }

    static function showRegister() {
        require_once dirname(__FILE__) . '/../view/register.php';

        return;
    }

    static function login() {
        $error = AuthenticationLoginValidator::validate($_POST);

        if (sizeof($error) > 0) {
            $__error = $error;
            
            require_once dirname(__FILE__) . '/../view/login.php';

            return;
        }

        try {
            $repository = new UserRepository();
            $user = $repository->find($_POST['email']);

            if ($user) {
                if (password_verify($_POST['password'], $user->password)) {
                    $_SESSION['user_handle'] = $user->handle;
                    header('Location: /home');
                } else {
                    $__error = [ 'general' => 'The email or password are incorrect.' ];
    
                    require_once dirname(__FILE__) . '/../view/login.php';
    
                    return;
                }
            }
        } catch (ErrorException $e) {
            $__error = [ '__global' => $e->getMessage() ];

            require_once dirname(__FILE__) . '/../view/login.php';

            return;
        }
    }

    static function logout() {
        session_destroy();

        header('Location: /');

        return;
    }

    static function register() {
        $error = AuthenticationRegisterValidator::validate($_POST);

        if (sizeof($error) > 0) {
            $__error = $error;
            
            require_once dirname(__FILE__) . '/../view/register.php';

            return;
        }

        $_POST['password'] = password_hash($_POST['password'], PASSWORD_DEFAULT);
        $user = new UserModel($_POST);

        try {
            $repository = new UserRepository();
            $repository->save($user);

            $_SESSION['user_handle'] = $user->handle;
        } catch (Exception $e) {
            $__error = [ '__global' => $e->getMessage() ];

            require_once dirname(__FILE__) . '/../view/register.php';

            return;
        }

        header('Location: /home');
    }
}
