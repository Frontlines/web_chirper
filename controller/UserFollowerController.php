<?php

namespace Controller;

use Repository\UserFollowerRepository;
use Repository\UserRepository;
use Model\UserFollowerModel;

class UserFollowerController {
    static function follow($matches) {
        if (!isset($_SESSION['user_handle'])) {
            // @todo
            throw "unauthorized";
        }

        $user_repository = new UserRepository();
        $follower_user = $user_repository->find($_SESSION['user_handle']);
        $followee_user = $user_repository->find($matches[0]);

        if (!$follower_user || !$followee_user) {
            // @todo
            throw "no user";
        }

        $user_follower = new UserFollowerModel([
            'follower_user' => $follower_user,
            'followee_user' => $followee_user,
        ]);

        $user_follower_repository = new UserFollowerRepository();
        $user_follower_repository->save($user_follower);

        header('Location: /' . $followee_user->handle);

        return;
    }

    static function unfollow($matches) {
        if (!isset($_SESSION['user_handle'])) {
            // @todo
            throw "unauthorized";
        }

        $user_repository = new UserRepository();
        $follower_user = $user_repository->find($_SESSION['user_handle']);
        $followee_user = $user_repository->find($matches[0]);

        if (!$follower_user || !$followee_user) {
            // @todo
            throw "no user";
        }

        $user_follower_repository = new UserFollowerRepository();
        $user_follower_repository->removeByUsers($follower_user, $followee_user);

        header('Location: /' . $followee_user->handle);

        return;
    }

    static function showFollowers($matches) {
        if (!$matches || empty($matches)) {
            header('Location: /404');

            return;
        }

        $handle = $matches[0];

        $user_repository = new UserRepository();
        $user = $user_repository->find($handle);

        if (!$user) {
            header('Location: /404');

            return;
        }

        $is_followed = false;

        $user_follower_repository = new UserFollowerRepository();
        $follower_count = count($user_follower_repository->allFollowingUser($user));
        $followee_count = count($user_follower_repository->allFollowedByUser($user));

        if (isset($_SESSION['user_handle'])) {
            $auth_user = $user_repository->find($_SESSION['user_handle']);
            $user_follower = $user_follower_repository->findFollowedByUser($auth_user, $user);
            $is_followed = boolval($user_follower);
        }

        $user_followers = $user_follower_repository->allFollowingUser($user);

        $__view = [
            'user' => $user,
            'is_authenticated' => isset($_SESSION['user_handle']),
            'is_authenticated_profile' => isset($_SESSION['user_handle']) && $_SESSION['user_handle'] === $user->handle,
            'is_home' => false,
            'followers' => $user_followers,
            'is_followed' => $is_followed,
            'follower_count' => $follower_count,
            'followee_count' => $followee_count,
        ];

        require_once dirname(__FILE__) . '/../view/followers.php';

        return;
    }

    static function showFollowing($matches) {
        if (!$matches || empty($matches)) {
            header('Location: /404');

            return;
        }

        $handle = $matches[0];

        $user_repository = new UserRepository();
        $user = $user_repository->find($handle);

        if (!$user) {
            header('Location: /404');

            return;
        }

        $user_follower_repository = new UserFollowerRepository();
        $follower_count = count($user_follower_repository->allFollowingUser($user));
        $followee_count = count($user_follower_repository->allFollowedByUser($user));

        $is_followed = false;

        if (isset($_SESSION['user_handle'])) {
            $auth_user = $user_repository->find($_SESSION['user_handle']);
            $user_follower = $user_follower_repository->findFollowedByUser($auth_user, $user);
            $is_followed = boolval($user_follower);
        }

        $user_followers = $user_follower_repository->allFollowedByUser($user);

        $__view = [
            'user' => $user,
            'is_authenticated' => isset($_SESSION['user_handle']),
            'is_authenticated_profile' => isset($_SESSION['user_handle']) && $_SESSION['user_handle'] === $user->handle,
            'is_home' => false,
            'followees' => $user_followers,
            'is_followed' => $is_followed,
            'follower_count' => $follower_count,
            'followee_count' => $followee_count,
        ];

        require_once dirname(__FILE__) . '/../view/following.php';

        return;
    }
}
