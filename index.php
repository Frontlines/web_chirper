<?php

session_start();

error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once __DIR__ . '/require.php';

$request = $_SERVER['REQUEST_URI'];
$request_path = parse_url($request, PHP_URL_PATH);
$request_method = $_SERVER['REQUEST_METHOD'];

$routes = [
    [
        'path' => '/^\/?$/',
        'method' => 'GET',
        'controller' => 'Controller\IndexController::show',
    ],
    [
        'path' => '/^\/404\/?$/',
        'method' => 'GET',
        'controller' => 'Controller\NotFoundController::show',
    ],
    [
        'path' => '/^\/login\/?$/',
        'method' => 'GET',
        'controller' => 'Controller\AuthenticationController::showLogin',
    ],
    [
        'path' => '/^\/login\/?$/',
        'method' => 'POST',
        'controller' => 'Controller\AuthenticationController::login',
    ],
    [
        'path' => '/^\/logout\/?$/',
        'method' => 'GET',
        'controller' => 'Controller\AuthenticationController::logout',
    ],
    [
        'path' => '/^\/register\/?$/',
        'method' => 'GET',
        'controller' => 'Controller\AuthenticationController::showRegister',
    ],
    [
        'path' => '/^\/register\/?$/',
        'method' => 'POST',
        'controller' => 'Controller\AuthenticationController::register',
    ],
    [
        'path' => '/^\/post\/?$/',
        'method' => 'POST',
        'controller' => 'Controller\PostController::create',
    ],
    [
        'path' => '/^\/home\/?$/',
        'method' => 'GET',
        'controller' => 'Controller\ProfileController::showHome',
    ],
    [
        'path' => '/^\/(.*)\/followers\/?$/',
        'method' => 'GET',
        'controller' => 'Controller\UserFollowerController::showFollowers',
    ],
    [
        'path' => '/^\/(.*)\/following\/?$/',
        'method' => 'GET',
        'controller' => 'Controller\UserFollowerController::showFollowing',
    ],
    [
        'path' => '/^\/(.*)\/follow\/?$/',
        'method' => 'POST',
        'controller' => 'Controller\UserFollowerController::follow',
    ],
    [
        'path' => '/^\/(.*)\/unfollow\/?$/',
        'method' => 'POST',
        'controller' => 'Controller\UserFollowerController::unfollow',
    ],
    [
        'path' => '/^\/(.*)\/?$/',
        'method' => 'GET',
        'controller' => 'Controller\ProfileController::show',
    ],
];

foreach ($routes as $route) {
    $matches = [];

    if (preg_match($route['path'], $request_path, $matches)) {
        if ($request_method !== $route['method']) {
            continue;
        }

        // remove matched whole string from matches, keeping capture groups
        $arguments = array_slice($matches, 1);

        $route['controller']($arguments);

        die();
    }
}

NotFoundController::show();
