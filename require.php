<?php

require_once __DIR__ . '/controller/AuthenticationController.php';
require_once __DIR__ . '/controller/IndexController.php';
require_once __DIR__ . '/controller/NotFoundController.php';
require_once __DIR__ . '/controller/PostController.php';
require_once __DIR__ . '/controller/ProfileController.php';
require_once __DIR__ . '/controller/UserFollowerController.php';
require_once __DIR__ . '/model/PostModel.php';
require_once __DIR__ . '/model/UserFollowerModel.php';
require_once __DIR__ . '/model/UserModel.php';
require_once __DIR__ . '/repository/PostRepository.php';
require_once __DIR__ . '/repository/UserFollowerRepository.php';
require_once __DIR__ . '/repository/UserRepository.php';
require_once __DIR__ . '/validator/AuthenticationLoginValidator.php';
require_once __DIR__ . '/validator/AuthenticationRegisterValidator.php';
require_once __DIR__ . '/validator/PostCreateValidator.php';
require_once __DIR__ . '/utility/Connection.php';
