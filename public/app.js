$(document).ready(function () {
    $('.js-form-post').find('button[type="submit"]').attr('disabled', 'disabled');
    $('.js-form-post-counter').html($('.js-form-post').find('[name="body"]').attr('maxlength'));
    $('.js-form-post').find('[name="body"]').on('keyup', function (e) {
        $('.js-form-post-counter').html($('.js-form-post').find('[name="body"]').attr('maxlength') - e.target.value.length);
        if (e.target.value.length < $('.js-form-post').find('[name="body"]').attr('minlength') || e.target.value.length > $('.js-form-post').find('[name="body"]').attr('maxlength')) {
            $('.js-form-post').find('button[type="submit"]').attr('disabled', 'disabled');
        } else {
            $('.js-form-post').find('button[type="submit"]').removeAttr('disabled');
        }
    })
});
